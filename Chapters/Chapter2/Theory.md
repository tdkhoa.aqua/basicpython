<!-- VARIABLES AND DATA TYPES: -->

# 1.Variables.

_Variables in Python are used to store data. They act as containers that hold values which can be accessed and manipulated throughout the program. When you assign a value to a variable, you're essentially storing that value in memory with a label (the variable name) attached to it, making it easy to reference later in the code._
**For example:**

`x = 10`
`name = "John"`
`is_admin = True`

# 2.DATA TYPES: integers, floats, strings, and booleans.

**Python supports various data types, including integers, floats, strings, and booleans.**

- **Integers**: Whole numbers without any decimal point.
- **Floats**: Numbers with decimal points.
- **Strings**: Ordered sequence of characters enclosed within single or double quotes.
- **Booleans**: Represents truth values, either True or False.x = 10 # integer
  **For example:**

  `y = 3.14 # float`
  `name = "John" # string`
  `is_admin = True # boolean`

# 3.Basic Arithmetic Operations:

**Python supports basic arithmetic operations such as addition, subtraction, multiplication, division, exponentiation, and modulus.**
`x = 10`
`y = 5`
`sum = x + y         # Addition`
`difference = x - y  # Subtraction`
`product = x * y     # Multiplication`
`quotient = x / y    # Division`
`power = x ** y      # Exponentiation`
`remainder = x % y   # Modulus (remainder)`

# 5.(PEMDAS/BODMAS).

**In Python, arithmetic expressions follow the order of operations, commonly known as PEMDAS (Parentheses, Exponents, Multiplication and Division - left to right, Addition and Subtraction - left to right) or BODMAS (Brackets, Orders, Division and Multiplication - left to right, Addition and Subtraction - left to right). This determines the sequence in which operations are performed.**

`result = 2 + 3 * 4  # Multiplication is performed before addition (PEMDAS)`

# 6.Print Statement and Formatting Strings:

**The print() function in Python is used to display output. You can format strings using the format() method or f-strings (formatted string literals) to include variables and other values within strings.**
`print("My name is Khoa and I am 24 years old.")`

# 7.Format strings using the format() method or f-strings (formatted string literals).

**Formatted String Literals in Python: Formatted string literals, also known as /f-strings/, allow you to embed expressions inside string literals, which are evaluated at runtime. They are denoted by the prefix f before the opening quote of the string. You can directly insert variables and expressions inside curly braces {} within the string, making it easy to include variables and format them within the string.**

- `name = "John"`
  `age = 30`
  `print("My name is {} and I am {} years old.".format(name, age))`

- `name = "John"`
  `age = 30`
  `print(f"My name is {name} and I am {age} years old.")`

# 8.Comments:

- # This is a single-line comment

- '''
  This is a
  multi-line
  comment
  '''

- Command Shift /
