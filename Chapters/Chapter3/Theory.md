# 1. Conditional Statements (if, elif, else):

_Conditional statements allow your program to make decisions based on the evaluation of expressions. They execute different blocks of code depending on whether the condition is true or false. If statements in Python are used for decision-making. They evaluate a condition and execute a block of code if the condition is true._

<!--
x = 10
if x > 5:
    print("x is greater than 5")
elif x == 5:
    print("x is equal to 5")
else:
    print("x is less than 5") -->

# 4. The truthiness in Python.

_In Python, many values are considered "truthy" if they are not false, including non-zero numbers and non-empty containers._

<!--
x = 10
if x:
    print("x is True") -->

# 5. Comparison and Logical Operators:

_Comparison operators (`==`, `!=`, `<`, `>`, `<=`, `>=`) are used to compare values. Logical operators (`and`, `or`, `not`) are used to combine conditions._

<!-- x = 5
if x != 10:
    print("x is not equal to 10") -->

<!--
x = 5
y = 10
if x == 5 and y == 10:
    print("Both conditions are True") -->

# 8. Indentation and Block Structure:

_In Python, indentation is used to define blocks of code. It is crucial for code readability and determines which statements are part of a particular block._

<!-- if x > 5:
    print("x is greater than 5")
    y = x * 2
    print("y is", y) -->

## In this code:

- a. The block of code within the if statement starts with the line print("x is greater than 5") and ends with the line print("y is", y).
- b. Both print statements and the assignment y = x \* 2 are indented with four spaces. This indentation indicates that they are part of the block that executes if x > 5.

**Indentation in Python is not just for aesthetics; it's a fundamental part of the language's syntax. Incorrect indentation can lead to syntax errors or unintended behavior.**
**The use of colons `:` to denote the start of an indented block. Colons are used in Python to signify the beginning of an indented block, such as in the case of if statements, loops, and function definitions.**

# 11. Nested Conditionals:

_Nested conditionals involve placing one if statement inside another. This allows for more complex decision-making processes._

<!--
x = 10
if x > 5:
if x < 15:
    print("x is between 5 and 15") -->

# 13. Loops (`for` and `while`):

_Loops are used to execute a block of code repeatedly. Python supports both `for` and `while` loops._

- The `for` loop is used to iterate over sequences such as lists, tuples, and strings.

<!--
for char in "Python":
    print(char) -->

- The `while` loop continues executing a block of code as long as the specified condition remains true.

<!--
x = 0
while x < 5:
    print(x)
    x += 1 -->

# 16. Loop control with `break` and `continue`.

_`break` is used to exit the loop prematurely, while `continue` skips the current iteration and continues with the next one._

_Using break to exit the loop prematurely_

<!--
for i in range(10):
    if i == 5:
        break  # Exit the loop when i reaches 5
    print(i)

# Output:
# 0
# 1
# 2
# 3
# 4 -->

_Using continue to skip the current iteration._

<!--
for i in range(5):
    if i == 3:
        continue  # Skip the current iteration when i is 3
    print(i)

# Output:
# 0
# 1
# 2
# 4 -->

<!-- # 17. Range Function:

The `range()` function generates a sequence of numbers. It is commonly used with `for` loops to iterate a specific number of times.

# 18. The range() function for generating sequences of numbers.

Example provided in earlier response.

# 19. How to use range() with for loops for iterating a specific number of times.

Example provided in earlier response.

# 20. Iterating through Lists and Dictionaries:

For loops can iterate through the elements in lists, tuples, and strings. Dictionaries can also be iterated through using loops.

# 21. Show how to use for loops to iterate through elements in a list.

Example provided in earlier response.

# 22. Looping through dictionary keys, values, and key-value pairs.

Example provided in earlier response.

# 23. List Comprehensions:

List comprehensions provide a concise way to create lists in Python.

# 24. The concept of list comprehensions for concise creation of lists.

Example provided in earlier response.

# 25. The syntax and advantages of list comprehensions.

List comprehensions offer a more compact syntax compared to traditional looping constructs, making code more readable and expressive.

# 26. Error Handling (try, except, finally):

Error handling allows you to deal with errors gracefully during the execution of a program.

# 27. The basics of exception handling using try, except, and finally blocks.

Example provided in earlier response.

# 28. How to catch specific exceptions and handle errors gracefully.

Example provided in earlier response.

# 29. Flow Control with Break and Continue:

Break and continue statements allow for more precise control over loop execution.

# 30. The use of break to exit a loop prematurely.

Example provided in earlier response.

# 31. The `continue` to skip the rest of the loop and continue with the next iteration.

Example provided in earlier response.

# 32. Pass Statement:

The pass statement in Python is used as a placeholder when no action is required.

# 33. The pass statement as a placeholder for empty code blocks.

Example provided in earlier response.

# 34. Switch Statements (not available in Python):

Python does not have a built-in switch statement, but alternative approaches using dictionaries or if-elif-else constructs can achieve similar functionality.

# 35. Python does not have a built-in switch statement.

Explanation provided in the title.

# 36. Alternative approaches using dictionaries or if-elif-else constructs. -->
