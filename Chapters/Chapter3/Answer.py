# # 1. Conditional Statements (if, elif, else):

# Write a Python program that takes an input from the user and prints whether the number is positive, negative, or zero using conditional statements (if, elif, else).

# x = float(input("Enter any number:"))
# if x > 0:
#     print("x is positive")
# elif x < 0:
#     print("x is negative")
# else:
#     print("x = 0")


# # 2. The truthiness in Python:

# Create a Python script that checks if a given list is empty or not. If the list is empty, print "The list is empty", otherwise print "The list is not empty".

# a = [1, 2, 3, 4, 5]

# if a:
#     print("The list is not empty")
# else:
#     print("The list is empty")

# # 3. Comparison and Logical Operators:

# Write a Python function that takes two numbers as input and returns "Equal" if they are equal, "Not equal" if they are not equal, and "Greater" if the first number is greater than the second number.

# # 4. Indentation and Block Structure:

# Create a Python script that calculates the factorial of a given number. Ensure that the calculation part is properly indented within a function definition.
x = float(input("Enter any  number:"))

# if
# # 5. Nested Conditionals:

# Write a Python program that takes a number as input and prints whether it is a positive number, negative number, or zero. If the number is positive, check if it is also an even number.

# x = float(input("Enter any number:"))
# if x > 0:
#     if x % 2 == 0:
#         print("x is positive and even number")
# elif x < 0:
#     print("x is negative")
# else:
#     print("x = 0")

# # 6. Loops (for and while):

# Write a Python script that prints all the even numbers from 0 to 20 using a for loop.
