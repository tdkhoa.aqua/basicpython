# Variables:

# 1. **Assignment 1: Track Inventory**

#    - You are managing inventory for a store. Declare variables to store the quantity of different items such as `apples`, `bananas`, and `oranges`.
#    - Initialize these variables with appropriate initial quantities.
#    - Print a message stating the current inventory status.

# apple_q = 10
# banana_q = 10
# orange_q = 5
# print(f"My store has {apple_q}  apples {banana_q} bananas {orange_q} oranges")

# 2. **Assignment 2: User Information**

#    - Ask the user to enter their name, age, and email address.
#    - Store this information in variables `name`, `age`, and `email`.
#    - Print a greeting message including the user's name and age.

# 3. **Assignment 3: Calculate Area**

#    - Declare variables `length` and `width` to store the dimensions of a rectangle.
#    - Calculate the area of the rectangle and store it in a variable `area`.
#    - Print a message displaying the calculated area.

# 4. **Assignment 4: Convert Temperature**

#    - Declare a variable `celsius` to store a temperature in Celsius.
#    - Convert this temperature to Fahrenheit and store it in a variable `fahrenheit`.
#    - Print a message displaying both the Celsius and Fahrenheit temperatures.

#    **Formula**:

#    - Fahrenheit = Celsius × 9/5 + 32


# c = int(input("please enter your Celsius:"))
# f = int(c * 9/5 + 32)
# print(f"Celsius = {c} Fahrenheit = {f}")

# 5. **Assignment 5: Billing Calculation**
#    - Declare variables `price` and `quantity` to store the price and quantity of an item.
#    - Calculate the total cost of the items and store it in a variable `total_cost`.
#    - Print a message displaying the total cost.

# ### Data Types:

# 1. **Assignment 1: Contact Information**

#    - Declare variables to store the contact information of a person such as name, age, email, and phone number.
#    - Use appropriate data types for each variable.
#    - Print a message displaying all the contact information.


# 2. **Assignment 2: Student Scores**

#    - Declare variables to store the scores of a student in different subjects such as Math, Science, and English.
#    - Use appropriate data types for the scores.
#    - Calculate the average score and store it in a variable `average_score`.
#    - Print a message displaying the average score.

# 3. **Assignment 3: Product Details**

#    - Declare variables to store the details of a product such as name, price, quantity, and availability.
#    - Use appropriate data types for each variable.
#    - Print a message displaying all the product details.
# N = str(input("Enter product:"))
# P = float(input("Enter price:"))
# Q = int(input("Enter quantity:"))
# B = int(input("Enter quanlity need buy:"))
# A = Q >= B
# print(f"Product {N}: {P} euros, quantity {Q} and {A}")


# 4. **Assignment 4: Book Information**

#    - Declare variables to store the information of a book such as title, author, genre, publication year, and number of pages.
#    - Use appropriate data types for each variable.
#    - Print a message displaying all the book information.

# 5. **Assignment 5: Weather Forecast**
#    - Declare variables to store the weather forecast for a week such as temperature, humidity, and precipitation chance for each day.
#    - Use appropriate data types for each variable.
#    - Print a message displaying the weather forecast for the week.

# ### Basic Arithmetic Operations:

# 1. **Assignment 1: Shopping Cart Total**

#    - Declare variables to store the prices of different items in a shopping cart.
#    - Calculate the total cost of the items including tax and shipping.
#    - Print a message displaying the total cost.

# 2. **Assignment 2: Circle Area and Circumference**

#    - Declare a variable to store the radius of a circle.
#    - Calculate the area and circumference of the circle.
#    - Print a message displaying both the area and circumference.

#    **Formulas**:

#    - Area = π × radius^2
#    - Circumference = 2 × π × radius
# import math
# pi = math.pi
# R = float(input("Enter radius:"))
# A = int(pi * R ** 2)
# C = int(2 * pi * R)

# print(f"Area is {A} and Circumference is {C}")

# 3. **Assignment 3: Distance and Time**

#    - Declare variables to store the distance traveled by a car and the time taken.
#    - Calculate the average speed of the car.
#    - Print a message displaying the average speed.

#    **Formula**:

#    - Average Speed = Distance / Time

# 4. **Assignment 4: Body Mass Index (BMI)**

#    - Declare variables to store the weight (in kilograms) and height (in meters) of a person.
#    - Calculate the BMI using the formula: BMI = weight / (height^2).
#    - Print a message displaying the BMI.

# 5. **Assignment 5: Recipe Scaling**
#    - Declare variables to store the ingredients and quantities required for a recipe.
#    - Scale the quantities based on the number of servings required.
#    - Print a message displaying the scaled quantities for each ingredient.

# ### (PEMDAS/BODMAS):

# 1. **Assignment 1: Temperature Conversion**

#    - Declare variables to store temperatures in Celsius and Fahrenheit.
#    - Convert temperatures between Celsius and Fahrenheit using the conversion formulas.
#    - Print a message displaying both the Celsius and Fahrenheit temperatures.

# 2. **Assignment 2: Loan Calculation**

#    - Declare variables to store the principal amount, interest rate, and duration of a loan.
#    - Calculate the monthly payment using the formula for the loan repayment.
#    - Print a message displaying the monthly payment.

# 3. **Assignment 3: Geometric Shape Area**

#    - Declare variables to store the dimensions of geometric shapes such as a rectangle, triangle, and circle.
#    - Calculate the areas of these shapes.
#    - Print a message displaying the areas.

# 4. **Assignment 4: Stock Investment Return**

#    - Declare variables to store the initial investment amount, annual return rate, and investment duration.
#    - Calculate the total return on investment using the compound interest formula.
#    - Print a message displaying the total return on investment.

# 5. **Assignment 5: Recipe Cost Calculation**
#    - Declare variables to store the ingredients and their costs for a recipe.
#    - Calculate the total cost of the recipe based on the quantities required for each ingredient.
#    - Print a message displaying the total cost of the recipe.
############################################################
# Assignment: Write a Python program to calculate the area of a rectangle. Prompt the user to enter the length and width of the rectangle, calculate the area, and print the result.
L = float(input("Enter the length:"))
W = float(input("Enter the width:"))
A = L*W
print(f"The area of a rectangle is {A}")
