# Chapter 2: Python Basics

# Assignment: Write a Python program to calculate the area of a rectangle. Prompt the user to enter the length and width of the rectangle, calculate the area, and print the result.

# Chapter 3: Control Flow

# Assignment: Write a Python program to check if a number entered by the user is even or odd. Use control flow statements to print an appropriate message.

# Chapter 4: Data Structures

# Assignment: Write a Python program to find the largest element in a list. Create a list of integers, iterate through the list, and keep track of the largest element found.

# Chapter 5: Functions

# Assignment: Write a Python function to check if a given number is prime or not. The function should take an integer as input and return True if it's prime, otherwise False.

# Chapter 6: Modules and Packages

# Assignment: Create a Python module named calculator that contains functions for addition, subtraction, multiplication, and division. Import this module into another Python script and use these functions to perform basic arithmetic operations.

# Chapter 7: File Handling

# Assignment: Write a Python program to read a text file named input.txt line by line and count the number of lines in the file. Print the total number of lines at the end.

# Chapter 8: Exception Handling

# Assignment: Write a Python program that asks the user to enter two numbers and performs division. Handle the ZeroDivisionError exception if the second number is zero and print an appropriate error message.

# Chapter 9: Basic Projects

# Assignment: Implement a simple to-do list manager using Python. Allow the user to add tasks, mark tasks as completed, and view the list of tasks.

# Chapter 10: Additional Topics (Optional)

# Assignment: Write a Python program to generate Fibonacci series up to a specified number of terms. Use functions and recursion to implement this.

# Chapter 11: Practice and Projects

# Assignment: Encourage your student to work on a small project of their choice using Python. It could be a game, a web scraper, a data analysis tool, etc. Provide guidance and support as needed.

